/*
* Author: Pedro Miranda
*
*  graph implementation
*/


#include "graph.h"

/*graph structure*/
struct _graph
{
	int numVect; /*number of vertices*/
	int maxSqr; /*number of squares*/
	int* perfSqrs; /*vector of perfect squares*/
	int** edges; /*matrix of edges*/
};



/*
InitGraph: allocates memory for the graph and initializates n, sqr and perfSqrs values
*/
Graph *initGraph(int number)
{
	Graph* new_graph = NULL;
	int i=0;

	/*Allocate memory*/
	new_graph = (Graph*) calloc(1, sizeof(Graph));
	if(new_graph == NULL) /*check for errors*/
	{
		fprintf(stderr, "Out of memory...Exiting\n");
		return NULL;
	}

	/*initialize values*/
	new_graph->numVect = number;
	new_graph->maxSqr = (int)ceil(sqrt((double) (2*new_graph->numVect - 1)));
	new_graph->perfSqrs = (int*)calloc(new_graph->maxSqr, sizeof(int));
	if(new_graph->perfSqrs == NULL) /*check for errors*/
		{
			printf("Not enough memory..Exiting\n");
			free(new_graph);
			return NULL;
		}

	/*fill perfSqr vector*/
	for(i=0;i<new_graph->maxSqr;i++)
	{
		new_graph->perfSqrs[i] = (int)pow((double)(i+1), (double)2);
	}

	if(buildEdges(new_graph))
	{
		fprintf(stdout, "Graph created and initialized\n");
		return new_graph;
	}

	else 
		return NULL;
}

/*
deleteGraph: deletes the graph and all its contents
*/
void deleteGraph(Graph* graph)
{
	int i;

	if(graph != NULL)
	{
		/*delete the edges*/
		if(graph->edges != NULL)
		{
			for(i=0; i<=graph->numVect;i++)
			{
				if(graph->edges[i] != NULL)
					free(graph->edges[i]);
			}
		}
		free(graph->edges);
		fprintf(stdout, "Deleted graph edges\n");

		/*delete perfSqrs*/
		if (graph->perfSqrs != NULL)
			free(graph->perfSqrs);

		/*delete the graph*/
		free(graph);
		fprintf(stdout, "Deleted graph\n");
	}
	
	return;
}



/*
buildEdges: connect the vertices of the graph
Return 1 if successfull; 0 otherwise
NOTES: filling edge matrix is n², it can me (n²)/2 if built column by column
*/
int buildEdges(Graph* graph)
{
	int i,j, diff=0;

	if (graph == NULL) /*check for valid graph*/
	{
		fprintf(stdout, "Graph does not exist\n");
		return 0;
	}

	/*allocate edge matrix*/
	graph->edges = (int**) malloc((graph->numVect +1)*sizeof(int*));
	if(graph->edges == NULL) /*check for erros*/
	{
		fprintf(stdout, "Out of memory..Exiting\n");
		return 0;
	}

	/*allocate each line*/
	for(i=0; i<=graph->numVect;i++)
	{
		graph->edges[i] = (int*)calloc(graph->maxSqr, sizeof(int));
		if(graph->edges[i] == NULL) /*check for erros*/
		{
			fprintf(stdout, "Out of memory..Exiting\n");
			return 0;
		}
	}

	/*fill edges*/
	for(i=1;i<=graph->numVect;i++)
	{
		for(j=0;j<graph->maxSqr;j++)
		{
			diff = graph->perfSqrs[j] - i;
			if(diff > graph->numVect) /*skip line if passes numVect*/
				break;
			
			if((diff < 0) ||(diff == i))
				graph->edges[i][j] = 0;
			else
				graph->edges[i][j] = diff;
		}
	}

	fprintf(stdout, "Edges created\n");

	return 1;
}


/*
printGraph: prints graph information into the console
*/
void printGraph(Graph* graph)
{
	int i, j;

	if (graph == NULL) /*check for valid graph*/
	{
		fprintf(stdout, "Graph does not exist\n");
		return;
	}

	fprintf(stdout, "Numvert = %d \t MaxSqrs = %d\n",graph->numVect, graph->maxSqr);

	fprintf(stdout, "PerfSqrs =");
	for(i=0;i<graph->maxSqr;i++)
	{
		fprintf(stdout, " %d -", graph->perfSqrs[i]);
	}

	fprintf(stdout, "\n");

	for(i=0;i<=graph->numVect;i++)
	{
		fprintf(stdout, "%d:\n", i);
		for (j=0;j<graph->maxSqr;j++)
		{
			fprintf(stdout, " %d -", graph->edges[i][j]);
		}
		fprintf(stdout, "\n");
	}
}

/*
get the number of vertices in the graph
returns -1 in case of NULL graph
*/
int GRAPH_getNumVect(Graph* graph)
{
	if(graph != NULL)
		return graph->numVect;
	else
		return -1;
}

/*
get the number of Perfect squares in the graph
returns -1 in case of NULL graph
*/
int GRAPH_getMaxSqr(Graph* graph)
{
	if(graph != NULL)
		return graph->maxSqr;
	else
		return -1;
}


/*
get the perfect square [i] in the graph
returns -1 in case of NULL graph
*/
int GRAPH_getPerfSqr(Graph* graph, int i)
{
	if(graph != NULL)
		return graph->perfSqrs[i];
	else
		return -1;
}

/*
get the edge[i][j] in the graph
returns -1 in case of NULL graph
*/
int GRAPH_getEdge(Graph* graph, int i, int j)
{
	if(graph != NULL)
	{
		if(graph->edges != NULL)
		{
			if(graph->edges[i] != NULL)
			{
				return graph->edges[i][j];
			}
			return -1;
		}
		return -1;	
	}	
	else
		return -1;
}