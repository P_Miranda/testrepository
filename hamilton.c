/*
Author: P Miranda

Hamilton Path implementation

*/

#include "hamilton.h"

/*node struct declaration*/
struct _node
{
	int node;
	Node* nextEdge;
};

/*
start a stack of nodes - UNUSED
*/
Node* STACKbegin()
{
	Node* newStack = NULL;

	newStack = (Node*) malloc(sizeof(Node));
	if(newStack == NULL)
	{
		fprintf(stderr, "Out of memory...Exiting\n");
		return NULL;
	}

	newStack->node = 0;
	newStack->nextEdge = NULL;

	return newStack;
}

/*
delete the entire stack
*/
void STACKdelete(Node* Stk)
{
	Node* aux = NULL;

	while(Stk!=NULL)
	{
		aux = Stk->nextEdge;
		free(Stk);
		Stk = aux;
	}

	return;
}

/*
insert a new node
*/
Node* STACKadd(Node* Stk, int i)
{
	/*create new edge struct*/
	Node* newEdge = (Node*) malloc(sizeof(Node));
	if(newEdge == NULL)
	{	
		fprintf(stderr, "Out of memory...Returning the same stack\n");
		return Stk; /*return the unchanged stack*/
	}

	/*initialize edge*/
	newEdge->node = i;
	newEdge->nextEdge = Stk;

	return newEdge;
}

/*
remove edge from Stack
returns -1 in case of no stack
*/
int STACKremove(Node** Stk_)
{
	Node* temp = NULL;
	int vert=0;

	if( (*Stk_) != NULL)
	{
		temp = (*Stk_)->nextEdge;
		vert = (*Stk_)->node;
		free(*Stk_);
		*Stk_ = temp;
		return vert;
	}
	else
		return -1;
}

/*
check if stack is empty: returns 1 if empty - 0 otherwise
*/
int STACKisEmpty(Node* Stk)
{
	return (Stk == NULL);
}

/*Hamilton path functions*/

/*
brute force hamiltonian path implementation
returns solution if success, NULL otherwhise
*/
int* DoYouKnowTheHamiltonianWae(Graph* g)
{
	/*variables*/
	int count=0, i,j;
	int currentNode = -1, prevNode = -1;
	int thisEdge=0;
	int *path = NULL, *branches = NULL, *solution = NULL;
	Node* Stk = NULL;

	/*appli for all starting nodes*/
	for(i=0;i<GRAPH_getNumVect(g);i++)
	{
		/*setup path vector - starts at 0*/
		path = (int*) calloc(GRAPH_getNumVect(g)+1, sizeof(int));
		if(path == NULL) /*check for errors*/
		{
			fprintf(stderr, "Out of memory...Exiting\n");
			return NULL;
		}

		/*setup path vector - starts at 0*/
		branches = (int*) calloc(GRAPH_getNumVect(g)+1, sizeof(int));
		if(branches == NULL) /*check for errors*/
		{
			/*free memory*/
			free(path);
			fprintf(stderr, "Out of memory...Exiting\n");
			return NULL;
		}

		/*add node to queue*/
		Stk = STACKadd(Stk, i);
		/*update variables*/
		count=1;
		path[i] = i;
		currentNode = i;

		fprintf(stdout, "Starting node %d\n",i+1);

		/*search loop*/
		while(!STACKisEmpty(Stk))
		{
			/*get a node*/
			prevNode = currentNode;
			currentNode = STACKremove(&Stk);
			/*update path*/
			path[currentNode] = prevNode;

			fprintf(stdout, "Current Node: %d\n", currentNode);


			/*check for solution*/
			if(count == GRAPH_getNumVect(g))
			{
				/*get solution from path*/
				solution = Hamilton_getSolution(g, path, currentNode);
				/*free memory*/
				free(path);
				free(branches);
				STACKdelete(Stk);

				return solution;
			}

			/*check for further nodes*/
			for(j=0; j<GRAPH_getMaxSqr(g);j++)
			{
				thisEdge = GRAPH_getEdge(g, currentNode, j);
				/*check if node is connected and off the path*/
				if( (thisEdge != 0) && (path[thisEdge] == 0))
				{
					/*add to queue*/
					Stk = STACKadd(Stk, thisEdge);
					/*update branches*/
					branches[currentNode]++;
				}
			}

			/*update count accordingly*/
			if(branches[currentNode] != 0) /*path can advance*/
			{
				count++;
			}
			else if(branches[prevNode] !=0) /*we can check a 'side' node*/
			{
				/*keep count the same*/
			}
			else /*unwind until there's nodes to check*/
			{
				while((branches[prevNode] == 0) && (prevNode != i)) /*stop if we got to the beggining*/
				{
					/*move back in the path*/
					path[currentNode] = 0;
					currentNode = prevNode;
					prevNode = path[prevNode];
					/*reduce counter*/
					count--;
				}
			}

			/*update branches*/
			branches[prevNode]--;

		}
		/*delete allocated memory*/
		free(path);
		free(branches);
		STACKdelete(Stk);
	}
	
	fprintf(stdout, "No solution was found.\n");

	/*if no solution was found*/
	return NULL;
}

/*
retrieves a solution from path vector
null in case of error
*/
int* Hamilton_getSolution(Graph* g, int* path, int end)
{
	int i=0, cnt=0;
	int* solution = NULL;

	solution = (int*) calloc(GRAPH_getNumVect(g)+1, sizeof(int));
	if(solution == NULL) /*check for errors*/
	{
		fprintf(stderr, "Out of memory...Exiting\n");
		return NULL;
	}

	/*unwind the path*/
	for(i=end, cnt=0;cnt<=GRAPH_getNumVect(g); i=path[i], cnt++)
	{
		/*put solution in vector*/
		solution[cnt] = i;
	}

	fprintf(stdout, "A solution was found!\n");

	return solution;
}

/*
print a vector contents
*/
void printVector(int* vector, int size)
{
	int i;
	if(vector != NULL)
	{
		fprintf(stdout, "\nVector:");
		for(i=0; i<size;i++)
			fprintf(stdout, "%d ",vector[i]);

		fprintf(stdout, "\n");
	}
	return;
}


/*a new atempt to find hamilton path in a intinger graph*/
int* findHamiltonPath(Graph*g)
{
	/*variables block*/
	int i, j, currentNode, prevNode, thisEdge, nodeCnt = 0;
	Node* Stk = NULL;
	int *path=NULL, *branches=NULL, *solution=NULL;

	/*loop for all starting nodes*/
	for(i=1;i<=GRAPH_getNumVect(g);i++)
	{
		/*start a new path and branches*/
		path = (int*) calloc(GRAPH_getNumVect(g)+1,sizeof(int));
		if(path == NULL)
		{
			fprintf(stdout, "Out of memory...Exiting\n");
			return NULL;
		}
		branches = (int*) calloc(GRAPH_getNumVect(g)+1, sizeof(int));
		if (branches == NULL)
		{
			fprintf(stdout, "Out of memory...Exiting\n");
			free(path);
			return NULL;
		}

		/*insert starting node in the stack*/
		Stk = STACKadd(Stk, i);
		/*reset node counter*/
		nodeCnt = 0;
		/*set prevNode*/
		prevNode = i;

		/*pathfinding loop*/
		while(!STACKisEmpty(Stk))
		{
			/*get next node from stack*/
			currentNode = STACKremove(&Stk);
			/*update nodeCounter*/
			nodeCnt++;
			/*update path*/
			path[currentNode] = prevNode;

			/*check if we have a solution*/
			if(nodeCnt == GRAPH_getNumVect(g))
			{
				/*get solution from path*/
				solution = Hamilton_getSolution(g, path, currentNode);
				/*free memory*/
				free(path);
				free(branches);
				STACKdelete(Stk);

				return solution;
			}

			/*check node for further branches*/
			for(j=0;j<GRAPH_getMaxSqr(g);j++)
			{
				/*get graph edge*/
				thisEdge = GRAPH_getEdge(g, currentNode, j);
				if(( path[thisEdge] == 0) && (thisEdge != 0) ) /*edges exist and node is not in the path*/
				{
					/*add node to the stack*/
					Stk = STACKadd(Stk, thisEdge);
					/*update branches*/
					branches[currentNode]++;
				}
			}

			/*check next step*/
			if( branches[currentNode] > 0) /*path can advance*/
			{
				/*update prevNode in advance*/
				prevNode = currentNode;
				/*reduce branches from this node*/
				branches[currentNode]--;
			}
			else if(branches[prevNode] > 0) /*path can go to a sideways node*/
			{
				/*reduce the node counter*/
				nodeCnt--;
				/*update path*/
				path[currentNode] = 0;
				/*reduce prevNode branches*/
				branches[prevNode]--;

			}
			else /*path has to go back*/
			{
				/*reduce count by 1 to compensate for the addition in the begining of the cycle*/
				nodeCnt--;

				/*search for the first node with branches*/
				while( (branches[prevNode] == 0) && (prevNode != path[prevNode]) )
				{
					/*update path*/
					path[currentNode] = 0;
					/*reduce nodeCnt*/
					nodeCnt--;
					/*update prevNode and currentNode*/
					currentNode = prevNode;
					prevNode = path[prevNode];
				}
				/*clear current node from path*/
				path[currentNode] = 0;
				/*update branches of the prevNode*/
				branches[prevNode]--;
			}
		}

		/*free alocated memory*/
		free(path);
		free(branches);
		STACKdelete(Stk);
	}
	/*no path was found*/
	return NULL;
}