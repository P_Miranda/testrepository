#include <stdio.h>
#include <stdlib.h>

#include "defs.h"
#include "graph.h"
#include "hamilton.h"


int main(int argc, char* argv[])
{
	/*variables block*/
	Graph* graph = NULL;
	int* solution=NULL;

	/*get number of edges*/
	if(argc >= 2)
		{
			if(atoi(argv[1]) > 0)
			{
				printf("Valid input value of: %d\n", atoi(argv[1]));
			}
			else
			{
				printf("Invalid input, please use a integer > 0.\n");
				exit(EXIT_SUCCESS);
			}
		}
	else
		{
			printf("Invalid input, please insert program arguments.\n");
			exit(EXIT_SUCCESS);
		}

	graph = initGraph(atoi(argv[1]));
	if(graph == NULL)
	{
		fprintf(stdout, "Graph was not created..Exiting\n");
		return EXIT_SUCCESS;
	}


	/*get solution*/
	solution = findHamiltonPath(graph);
	if(solution == NULL)
	{
		fprintf(stdout, "No solution found for size %d\n", GRAPH_getNumVect(graph));
	}
	else
	{
		printVector(solution, GRAPH_getNumVect(graph));
	} 
	/*print graph*/
	/*printGraph(graph);*/

	/*free memmory*/
	deleteGraph(graph);
	free(solution);

	printf("Program successfull!\n");
	return EXIT_SUCCESS;
}