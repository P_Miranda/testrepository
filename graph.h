/*
* Author: Pedro Miranda
* 
* Header file for graph.c
*/

#ifndef _GRAPH_H
#define _GRAPH_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <math.h>

	#include "defs.h"

	typedef struct _graph Graph;

	Graph *initGraph(int number);
	void deleteGraph(Graph* graph);
	int buildEdges(Graph* graph);
	void printGraph(Graph* graph);

	/*access data from other libraries*/
	int GRAPH_getNumVect(Graph* graph);
	int GRAPH_getMaxSqr(Graph* graph);
	int GRAPH_getPerfSqr(Graph* graph, int i);
	int GRAPH_getEdge(Graph* graph, int i, int j);
#endif