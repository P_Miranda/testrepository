/*
Author: P Miranda
Header for hamilton path functions

*/


#ifndef _HAMILTON_H
#define _HAMILTON_H

#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "graph.h"


/*edge stack implementation*/
typedef struct _node Node;

/*STACK functions*/
Node* STACKbegin();
void STACKdelete(Node* Stk);
Node* STACKadd(Node* Stk, int i);
int STACKremove(Node** Stk_);
int STACKisEmpty(Node* Stk);

/*Hamiltonian path functions*/
int* DoYouKnowTheHamiltonianWae(Graph* g);
int* Hamilton_getSolution(Graph * g, int* path, int end);

void printVector(int* vector, int size);

int* findHamiltonPath(Graph*g);


#endif