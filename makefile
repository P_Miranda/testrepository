#Compiler
CC = gcc

#Compiler Flags
CFLAGS = -Wall -ansi -O3 -g

#Sources
SOURCES = test.c hamilton.c graph.c 

#Objects
OBJECTS = test.o hamilton.o graph.o 


project: test

test: $(OBJECTS)
	$(CC) $(CFLAGS) -o programName $(OBJECTS) -lm

hamilton.o: hamilton.h hamilton.c

graph.o: graph.h graph.c

#Clean project directory
clean::
	rm -f *.o core a.out programName *~


depend::
	makedepend $(SOURCES)
# DO NOT DELETE